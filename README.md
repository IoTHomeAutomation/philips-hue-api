# Simple Philips Hue client

Simple Client used to control the Philips Hue lighting system.

## Use

1. Register app and get key (you need to know HUB IP address)

```php
$client = new \IoTHome\PhilipsHueApi\Hue\Client('10.0.0.5', null);

// register application
$key = $client->registerApplication('app-name');

// save key to your database and use it in a new instance of Client
```

2. Create client with provided key and now you can control your lights

```php
$client = new \IoTHome\PhilipsHueApi\Hue\Client('10.0.0.5', 'key');

// get all available lights
$lights = $client->getLights();

// Get light by id and turn it on
$light = $client->getLightById('abcd');
$light->setOn(true);
$client->updateLight($light);

// Get all available Rooms
$rooms = $client->getRooms();

// Get all available Zones
$rooms = $client->getZones();
```

Apply scene to lights:
```php
$client = new \IoTHome\PhilipsHueApi\Hue\Client('10.0.0.5', 'key');

$scene = $client->getSceneById('abcdef12-abcd-1234-93bb-da3e0ea601d1');

$service = new \IoTHome\PhilipsHueApi\Scenes\SceneService($client);

// Method applies scene to all lights in group
$service->applyScene($scene);
```