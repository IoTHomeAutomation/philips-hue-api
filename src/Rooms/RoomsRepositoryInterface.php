<?php

declare(strict_types=1);

namespace IoTHome\PhilipsHueApi\Rooms;

use IoTHome\PhilipsHueApi\Exceptions\RoomNotFoundException;

interface RoomsRepositoryInterface
{
    /**
     * @return Room[]
     */
    public function getAll(): array;

    /**
     * @param string $id
     * @return Room
     * @throws RoomNotFoundException
     */
    public function getById(string $id): Room;
}
