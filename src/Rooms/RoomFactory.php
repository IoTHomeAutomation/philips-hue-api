<?php

declare(strict_types=1);

namespace IoTHome\PhilipsHueApi\Rooms;

final class RoomFactory
{
    /**
     * @param array<mixed> $data
     * @return Room
     */
    public function createFromApiData(array $data): Room
    {
        $lightIds = [];

        if (isset($data['services']) && is_array($data['services'])) {
            foreach ($data['services'] as $service) {
                if (isset($service['rtype']) && isset($service['rid'])) {
                    if ($service['rtype'] === 'light') {
                        $lightIds[] = $service['rid'];
                    }
                }
            }
        }

        return new Room(
            (string)($data['id'] ?? ''),
            (string)($data['metadata']['name'] ?? ''),
            (string)($data['metadata']['archetype'] ?? ''),
            $lightIds
        );
    }

    /**
     * @param string $id
     * @param string $name
     * @param string $archetype
     * @param string[] $lightIds
     * @return Room
     */
    public function create(string $id, string $name, string $archetype, array $lightIds): Room
    {
        return new Room(
            $id,
            $name,
            $archetype,
            $lightIds
        );
    }
}
