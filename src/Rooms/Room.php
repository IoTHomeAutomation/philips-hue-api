<?php

declare(strict_types=1);

namespace IoTHome\PhilipsHueApi\Rooms;

use IoTHome\PhilipsHueApi\Lights\LightGroupInterface;

final class Room implements LightGroupInterface
{
    /**
     * @param string $id
     * @param string $name
     * @param string $archetype
     * @param string[] $lightIds
     */
    public function __construct(
        private readonly string $id,
        private readonly string $name,
        private readonly string $archetype,
        private readonly array $lightIds
    ) {
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getArchetype(): string
    {
        return $this->archetype;
    }

    /**
     * @return string[]
     */
    public function getLightIds(): array
    {
        return $this->lightIds;
    }
}
