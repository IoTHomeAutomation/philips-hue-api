<?php

declare(strict_types=1);

namespace IoTHome\PhilipsHueApi\Rooms;

use IoTHome\PhilipsHueApi\Client\APIClientInterface;
use IoTHome\PhilipsHueApi\Exceptions\RoomNotFoundException;

final class RoomsApiRepository implements RoomsRepositoryInterface
{
    public function __construct(
        private readonly APIClientInterface $apiClient
    ) {
    }

    /**
     * @return Room[]
     */
    public function getAll(): array
    {
        $response = $this->apiClient->readResource('room');

        if ($response->isSuccess()) {
            $rooms = [];

            $factory = new RoomFactory();

            $data = $response->getData();
            foreach ($data as $roomData) {
                if (is_array($roomData)) {
                    $rooms[] = $factory->createFromApiData($roomData);
                }
            }

            return $rooms;
        } else {
            return [];
        }
    }

    /**
     * @param string $id
     * @return Room
     * @throws RoomNotFoundException
     */
    public function getById(string $id): Room
    {
        $response = $this->apiClient->readResource('room', $id);

        if ($response->isSuccess()) {
            $factory = new RoomFactory();

            $data = $response->getData();
            $data = $data[0] ?? [];

            if (is_array($data)) {
                return $factory->createFromApiData($data);
            }
        }

        throw new RoomNotFoundException();
    }
}
