<?php

declare(strict_types=1);

namespace IoTHome\PhilipsHueApi\Scenes;

use IoTHome\PhilipsHueApi\Lights\Color;

final class Action
{
    public function __construct(
        private readonly string $lightId,
        private readonly bool $on,
        private readonly ?Color $color,
        private readonly ?int $colorTemperature,
        private readonly float $brightness,
    ) {
    }

    public function getLightId(): string
    {
        return $this->lightId;
    }

    public function isOn(): bool
    {
        return $this->on;
    }

    public function getColor(): ?Color
    {
        return $this->color;
    }

    public function getColorTemperature(): ?int
    {
        return $this->colorTemperature;
    }

    public function getBrightness(): float
    {
        return $this->brightness;
    }
}
