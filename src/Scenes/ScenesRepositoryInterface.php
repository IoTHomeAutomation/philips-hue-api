<?php

declare(strict_types=1);

namespace IoTHome\PhilipsHueApi\Scenes;

use IoTHome\PhilipsHueApi\Exceptions\SceneNotFoundException;

interface ScenesRepositoryInterface
{
    /**
     * @return Scene[]
     */
    public function getAll(): array;

    /**
     * @param string $id
     * @return Scene
     * @throws SceneNotFoundException
     */
    public function getById(string $id): Scene;
}
