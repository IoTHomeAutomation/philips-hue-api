<?php

declare(strict_types=1);

namespace IoTHome\PhilipsHueApi\Scenes;

use IoTHome\PhilipsHueApi\Lights\LightGroupInterface;

final class Scene
{
    /**
     * @param string $id
     * @param string $name
     * @param string|null $publicImageId
     * @param float $speed
     * @param LightGroupInterface $group
     * @param Action[] $actions
     */
    public function __construct(
        private readonly string $id,
        private readonly string $name,
        private readonly ?string $publicImageId,
        private readonly float $speed,
        private readonly LightGroupInterface $group,
        private readonly array $actions
    ) {
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getPublicImageId(): ?string
    {
        return $this->publicImageId;
    }

    public function getSpeed(): float
    {
        return $this->speed;
    }

    public function getGroup(): LightGroupInterface
    {
        return $this->group;
    }

    /**
     * @return Action[]
     */
    public function getActions(): array
    {
        return $this->actions;
    }
}
