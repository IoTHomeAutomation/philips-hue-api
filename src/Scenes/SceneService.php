<?php

declare(strict_types=1);

namespace IoTHome\PhilipsHueApi\Scenes;

use IoTHome\PhilipsHueApi\Hue\Client;
use IoTHome\PhilipsHueApi\Lights\Light;

final class SceneService
{
    public function __construct(
        private readonly Client $client
    ) {
    }

    /**
     * @param Scene $scene
     * @return void
     * @throws \IoTHome\PhilipsHueApi\Exceptions\LightNotFoundException
     * @throws \IoTHome\PhilipsHueApi\Exceptions\UpdateFailedException
     */
    public function applyScene(Scene $scene): void
    {
        $actions = $scene->getActions();
        $lights = $this->getLights($scene);

        foreach ($actions as $action) {
            if (isset($lights[$action->getLightId()])) {
                $light = $lights[$action->getLightId()];

                $light->setOn($action->isOn());

                if ($light->isSupportsBrightness()) {
                    $light->setBrightness($action->getBrightness());
                }

                if ($light->isSupportsColorTemperature()) {
                    $light->setColorTemperature($action->getColorTemperature());
                }

                if ($light->isSupportsColors() && $action->getColor() !== null) {
                    $light->setX($action->getColor()->getX());
                    $light->setY($action->getColor()->getY());
                }

                if (!empty($light->getChangedAttributes())) {
                    $this->client->updateLight($light);
                }
            }
        }
    }

    /**
     * @param Scene $scene
     * @return array<string, Light>
     */
    private function getLights(Scene $scene): array
    {
        $lightsIds = $scene->getGroup()->getLightIds();

        $allLights = $this->client->getLights();

        $lights = [];

        foreach ($allLights as $light) {
            if (in_array($light->getId(), $lightsIds)) {
                $lights[$light->getId()] = $light;
            }
        }

        return $lights;
    }
}
