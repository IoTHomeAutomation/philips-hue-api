<?php

declare(strict_types=1);

namespace IoTHome\PhilipsHueApi\Scenes;

use IoTHome\PhilipsHueApi\Client\APIClientInterface;
use IoTHome\PhilipsHueApi\Exceptions\InvalidSceneData;
use IoTHome\PhilipsHueApi\Exceptions\SceneNotFoundException;

final class ScenesApiRepository implements ScenesRepositoryInterface
{
    public function __construct(
        private readonly APIClientInterface $apiClient
    ) {
    }

    /**
     * @return Scene[]
     */
    public function getAll(): array
    {
        $response = $this->apiClient->readResource('scene');

        if ($response->isSuccess()) {
            $scenes = [];

            $factory = new SceneFactory();

            $data = $response->getData();
            foreach ($data as $sceneData) {
                if (is_array($sceneData)) {
                    try {
                        $scenes[] = $factory->createFromApiData($sceneData);
                    } catch (InvalidSceneData) {
                        continue;
                    }
                }
            }

            return $scenes;
        } else {
            return [];
        }
    }

    /**
     * @param string $id
     * @return Scene
     * @throws SceneNotFoundException
     */
    public function getById(string $id): Scene
    {
        $response = $this->apiClient->readResource('scene', $id);

        if ($response->isSuccess()) {
            $factory = new SceneFactory();

            $data = $response->getData();
            $data = $data[0] ?? [];

            if (is_array($data)) {
                try {
                    return $factory->createFromApiData($data);
                } catch (InvalidSceneData) {
                    throw new SceneNotFoundException();
                }
            }
        }

        throw new SceneNotFoundException();
    }
}
