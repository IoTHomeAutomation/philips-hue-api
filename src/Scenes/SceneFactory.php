<?php

declare(strict_types=1);

namespace IoTHome\PhilipsHueApi\Scenes;

use IoTHome\PhilipsHueApi\Exceptions\InvalidSceneData;
use IoTHome\PhilipsHueApi\Lights\Color;
use IoTHome\PhilipsHueApi\Rooms\RoomFactory;
use IoTHome\PhilipsHueApi\Zones\ZoneFactory;

final class SceneFactory
{
    /**
     * @param array<mixed> $data
     * @return Scene
     * @throws InvalidSceneData
     */
    public function createFromApiData(array $data): Scene
    {
        $actions = $data['actions'] ?? null;

        if (!is_array($actions)) {
            throw new InvalidSceneData();
        }

        $lightIds = [];
        $actionsToExecute = [];

        foreach ($actions as $action) {
            $target = $action['target'] ?? null;

            if (!is_array($target) || $target['rtype'] !== 'light' || !isset($target['rid'])) {
                throw new InvalidSceneData();
            }

            $lightId = (string)$target['rid'];

            $lightIds[] = $lightId;

            $actionData = $action['action'] ?? null;

            $color = null;

            if (isset($actionData['color']) && isset($actionData['color']['xy'])) {
                $color = new Color(
                    (float)($actionData['color']['xy']['x'] ?? 0),
                    (float)($actionData['color']['xy']['y'] ?? 0),
                );
            }

            $actionsToExecute[] = new Action(
                $lightId,
                (bool)($actionData['on']['on'] ?? true),
                $color,
                (isset($actionData['color_temperature']['mirek']) ?
                    (int)$actionData['color_temperature']['mirek'] : null),
                (isset($actionData['dimming']['brightness']) ?
                    (float)$actionData['dimming']['brightness'] : 0),
            );
        }

        $groupData = $data['group'] ?? null;
        $group = null;

        if (!is_array($groupData)) {
            throw new InvalidSceneData();
        }

        switch ($groupData['rtype'] ?? null) {
            case 'room':
                $roomFactory = new RoomFactory();
                $group = $roomFactory->create((string)$groupData['rid'], '', '', $lightIds);
                break;
            case 'zone':
                $zoneFactory = new ZoneFactory();
                $group = $zoneFactory->create((string)$groupData['rid'], '', '', $lightIds);
                break;
        }

        if ($group === null) {
            throw new InvalidSceneData();
        }

        return new Scene(
            (string)($data['id'] ?? ''),
            (string)($data['metadata']['name'] ?? ''),
            (string)($data['metadata']['image']['rid'] ?? ''),
            (float)($data['speed'] ?? 1),
            $group,
            $actionsToExecute
        );
    }
}
