<?php

declare(strict_types=1);

namespace IoTHome\PhilipsHueApi\Zones;

final class ZoneFactory
{
    /**
     * @param array<mixed> $data
     * @return Zone
     */
    public function createFromApiData(array $data): Zone
    {
        $lightIds = [];

        if (isset($data['services']) && is_array($data['services'])) {
            foreach ($data['services'] as $service) {
                if (isset($service['rtype']) && isset($service['rid'])) {
                    if ($service['rtype'] === 'light') {
                        $lightIds[] = $service['rid'];
                    }
                }
            }
        }

        return new Zone(
            (string)($data['id'] ?? ''),
            (string)($data['metadata']['name'] ?? ''),
            (string)($data['metadata']['archetype'] ?? ''),
            $lightIds
        );
    }

    /**
     * @param string $id
     * @param string $name
     * @param string $archetype
     * @param string[] $lightIds
     * @return Zone
     */
    public function create(string $id, string $name, string $archetype, array $lightIds): Zone
    {
        return new Zone(
            $id,
            $name,
            $archetype,
            $lightIds
        );
    }
}
