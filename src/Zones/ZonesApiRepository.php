<?php

declare(strict_types=1);

namespace IoTHome\PhilipsHueApi\Zones;

use IoTHome\PhilipsHueApi\Client\APIClientInterface;
use IoTHome\PhilipsHueApi\Exceptions\ZoneNotFoundException;

final class ZonesApiRepository implements ZonesRepositoryInterface
{
    public function __construct(
        private readonly APIClientInterface $apiClient
    ) {
    }

    /**
     * @return Zone[]
     */
    public function getAll(): array
    {
        $response = $this->apiClient->readResource('zone');

        if ($response->isSuccess()) {
            $zones = [];

            $factory = new ZoneFactory();

            $data = $response->getData();
            foreach ($data as $zoneData) {
                if (is_array($zoneData)) {
                    $zones[] = $factory->createFromApiData($zoneData);
                }
            }

            return $zones;
        } else {
            return [];
        }
    }

    /**
     * @param string $id
     * @return Zone
     * @throws ZoneNotFoundException
     */
    public function getById(string $id): Zone
    {
        $response = $this->apiClient->readResource('zone', $id);

        if ($response->isSuccess()) {
            $factory = new ZoneFactory();

            $data = $response->getData();
            $data = $data[0] ?? [];

            if (is_array($data)) {
                return $factory->createFromApiData($data);
            }
        }

        throw new ZoneNotFoundException();
    }
}
