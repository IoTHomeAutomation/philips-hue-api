<?php

declare(strict_types=1);

namespace IoTHome\PhilipsHueApi\Zones;

use IoTHome\PhilipsHueApi\Exceptions\ZoneNotFoundException;

interface ZonesRepositoryInterface
{
    /**
     * @return Zone[]
     */
    public function getAll(): array;

    /**
     * @param string $id
     * @return Zone
     * @throws ZoneNotFoundException
     */
    public function getById(string $id): Zone;
}
