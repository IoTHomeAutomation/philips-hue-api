<?php

declare(strict_types=1);

namespace IoTHome\PhilipsHueApi\Client;

use IoTHome\PhilipsHueApi\Exceptions\UnauthorizedUserException;
use IoTHome\PhilipsHueApi\Exceptions\UnknownHttpMethod;

final class CurlAPIClient implements APIClientInterface
{
    public function __construct(
        private readonly string $bridgeIPAddress,
        private readonly ?string $applicationKey
    ) {
    }

    /**
     * @param string $url
     * @param string $method
     * @param bool $authorise
     * @param null|array<mixed> $postData
     * @return APIResponse
     * @throws UnauthorizedUserException
     */
    private function callApi(
        string $url,
        string $method,
        bool $authorise,
        ?array $postData = null
    ): APIResponse {
        if ($authorise && empty($this->applicationKey)) {
            throw new UnauthorizedUserException();
        }

        $headers = [
            'Content-Type: application/json',
            'Host: ' . $this->bridgeIPAddress,
        ];

        if ($authorise) {
            $headers[] = 'hue-application-key: ' . $this->applicationKey;
        }

        $c = curl_init();

        curl_setopt($c, CURLOPT_URL, $url);
        curl_setopt($c, CURLOPT_CUSTOMREQUEST, $method);
        curl_setopt($c, CURLOPT_HEADER, false);
        curl_setopt($c, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($c, CURLOPT_CAINFO, __DIR__ . '/../Bridge/cacert.pem');
        curl_setopt($c, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($c, CURLOPT_TIMEOUT, 5);

        if (!empty($postData)) {
            $jsonData = json_encode($postData) ?: '';

            curl_setopt($c, CURLOPT_POSTFIELDS, $jsonData);

            $headers[] = 'Content-Length: ' . strlen($jsonData);
        }

        curl_setopt($c, CURLOPT_HTTPHEADER, $headers);

        $responseBody = curl_exec($c);
        $statusCode = curl_getinfo($c, CURLINFO_HTTP_CODE);
        $contentType = curl_getinfo($c, CURLINFO_CONTENT_TYPE);

        curl_close($c);

        $responseData = [];
        $success = false;

        if ($contentType === 'application/json' && is_string($responseBody)) {
            $responseData = json_decode($responseBody, true) ?: [];
        }

        if ($statusCode === 200) {
            $success = true;
        }

        if (is_array($responseData)) {
            // old /api returns data in different forms
            if (!isset($responseData['data']) && !isset($responseData['error'])) {
                if (isset($responseData[0]) && is_array($responseData[0])) {
                    if (isset($responseData[0]['data'])) {
                        $responseData['data'] = $responseData[0]['data'];
                    } elseif (isset($responseData[0]['success'])) {
                        $responseData['data'] = $responseData[0]['success'];
                    }

                    if (isset($responseData[0]['error'])) {
                        $responseData['error'] = $responseData[0]['error'];
                    }

                    unset($responseData[0]);
                }
            }

            $data = isset($responseData['data']) && is_array($responseData['data']) ? $responseData['data'] : [];
            $error = isset($responseData['error']) && is_array($responseData['error']) ? $responseData['error'] : [];
        } else {
            $data = [];
            $error = [];
        }

        return new APIResponse(
            $success,
            $data,
            $error,
        );
    }

    private function getResourceURL(string $resource): string
    {
        return $this->getApiURL('clip/v2/resource/' . $resource);
    }

    private function getApiURL(string $address): string
    {
        return 'https://' . $this->bridgeIPAddress . '/' . $address;
    }

    /**
     * @inheritDoc
     */
    public function readResource(string $resource, ?string $id = null, bool $authorise = true): APIResponse
    {
        $url = $this->getResourceURL($resource . ($id ? '/' . $id : ''));
        return $this->callApi($url, 'GET', $authorise);
    }

    /**
     * @inheritDoc
     */
    public function updateResource(string $resource, string $id, array $data, bool $authorise = true): APIResponse
    {
        $url = $this->getResourceURL($resource . '/' . $id);
        return $this->callApi($url, 'PUT', $authorise, $data);
    }

    /**
     * @inheritDoc
     */
    public function deleteResource(string $resource, string $id, bool $authorise = true): APIResponse
    {
        $url = $this->getResourceURL($resource . '/' . $id);
        return $this->callApi($url, 'DELETE', $authorise);
    }

    /**
     * @inheritDoc
     */
    public function createResource(string $resource, array $data, bool $authorise = true): APIResponse
    {
        $url = $this->getResourceURL($resource);
        return $this->callApi($url, 'POST', $authorise, $data);
    }

    /**
     * @inheritDoc
     */
    public function callAPIDirectly(string $address, string $method, ?array $data, bool $authorise = true): APIResponse
    {
        if (!in_array($method, ['GET', 'POST', 'PUT', 'DELETE'])) {
            throw new UnknownHttpMethod();
        }

        return $this->callApi($this->getApiURL($address), $method, $authorise, $data);
    }
}
