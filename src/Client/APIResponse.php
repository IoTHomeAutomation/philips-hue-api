<?php

declare(strict_types=1);

namespace IoTHome\PhilipsHueApi\Client;

final class APIResponse
{
    /**
     * @param bool $success
     * @param array<mixed> $data
     * @param array<mixed> $error
     */
    public function __construct(
        private readonly bool $success,
        private readonly array $data,
        private readonly array $error,
    ) {
    }

    /**
     * @return bool
     */
    public function isSuccess(): bool
    {
        return $this->success;
    }

    /**
     * @return array<mixed>
     */
    public function getData(): array
    {
        return $this->data;
    }

    /**
     * @return array<mixed>
     */
    public function getError(): array
    {
        return $this->error;
    }
}
