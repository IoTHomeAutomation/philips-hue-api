<?php

declare(strict_types=1);

namespace IoTHome\PhilipsHueApi\Client;

interface APIClientInterface
{
    /**
     * @param string $resource
     * @param array<mixed> $data
     * @param bool $authorise
     * @return APIResponse
     */
    public function createResource(string $resource, array $data, bool $authorise = true): APIResponse;

    /**
     * @param string $resource
     * @param string|null $id
     * @param bool $authorise
     * @return APIResponse
     */
    public function readResource(string $resource, ?string $id = null, bool $authorise = true): APIResponse;

    /**
     * @param string $resource
     * @param string $id
     * @param array<mixed> $data
     * @param bool $authorise
     * @return APIResponse
     */
    public function updateResource(string $resource, string $id, array $data, bool $authorise = true): APIResponse;

    /**
     * @param string $resource
     * @param string $id
     * @param bool $authorise
     * @return APIResponse
     */
    public function deleteResource(string $resource, string $id, bool $authorise = true): APIResponse;

    /**
     * @param string $address
     * @param string $method
     * @param null|array<mixed> $data
     * @param bool $authorise
     * @return APIResponse
     */
    public function callAPIDirectly(string $address, string $method, ?array $data, bool $authorise = true): APIResponse;
}
