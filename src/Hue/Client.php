<?php

declare(strict_types=1);

namespace IoTHome\PhilipsHueApi\Hue;

use IoTHome\PhilipsHueApi\Bridge\BridgeApiRepository;
use IoTHome\PhilipsHueApi\Bridge\BridgeRepositoryInterface;
use IoTHome\PhilipsHueApi\Client\CurlAPIClient;
use IoTHome\PhilipsHueApi\Lights\Light;
use IoTHome\PhilipsHueApi\Lights\LightsApiRepository;
use IoTHome\PhilipsHueApi\Lights\LightsRepositoryInterface;
use IoTHome\PhilipsHueApi\Rooms\Room;
use IoTHome\PhilipsHueApi\Rooms\RoomsApiRepository;
use IoTHome\PhilipsHueApi\Rooms\RoomsRepositoryInterface;
use IoTHome\PhilipsHueApi\Scenes\Scene;
use IoTHome\PhilipsHueApi\Scenes\ScenesApiRepository;
use IoTHome\PhilipsHueApi\Scenes\ScenesRepositoryInterface;
use IoTHome\PhilipsHueApi\Zones\Zone;
use IoTHome\PhilipsHueApi\Zones\ZonesApiRepository;
use IoTHome\PhilipsHueApi\Zones\ZonesRepositoryInterface;

class Client
{
    private LightsRepositoryInterface $lightsRepository;
    private RoomsRepositoryInterface $roomsRepository;
    private BridgeRepositoryInterface $bridgeRepository;
    private ZonesRepositoryInterface $zonesRepository;
    private ScenesRepositoryInterface $scenesRepository;

    public function __construct(
        private readonly string $hubAddress,
        private readonly ?string $applicationKey
    ) {
        if (!filter_var($hubAddress, FILTER_VALIDATE_IP)) {
            throw new \InvalidArgumentException($hubAddress . ' is not a valid IP address');
        }

        if (!extension_loaded('curl')) {
            throw new \BadFunctionCallException('The cURL extension is required.');
        }

        $apiClient = new CurlAPIClient($this->hubAddress, $this->applicationKey);

        $this->lightsRepository = new LightsApiRepository($apiClient);
        $this->roomsRepository = new RoomsApiRepository($apiClient);
        $this->bridgeRepository = new BridgeApiRepository($apiClient);
        $this->zonesRepository = new ZonesApiRepository($apiClient);
        $this->scenesRepository = new ScenesApiRepository($apiClient);
    }

    /**
     * @param string $applicationName
     * @return string
     * @throws \IoTHome\PhilipsHueApi\Exceptions\BridgeUnavailableException
     * @throws \IoTHome\PhilipsHueApi\Exceptions\LinkButtonNotPressedException
     * @throws \IoTHome\PhilipsHueApi\Exceptions\UnableToGetKeyException
     */
    public function registerApplication(string $applicationName): string
    {
        return $this->bridgeRepository->generateKey($applicationName);
    }

    public function isHubConnectedAndAuthenticated(): bool
    {
        return $this->bridgeRepository->isConnectedAndAuthenticated();
    }

    /**
     * @return Light[]
     */
    public function getLights(): array
    {
        return $this->lightsRepository->getAll();
    }

    /**
     * @param string $id
     * @return Light
     * @throws \IoTHome\PhilipsHueApi\Exceptions\LightNotFoundException
     */
    public function getLightById(string $id): Light
    {
        return $this->lightsRepository->getById($id);
    }

    /**
     * @param Light $light
     * @return Light
     * @throws \IoTHome\PhilipsHueApi\Exceptions\LightNotFoundException
     * @throws \IoTHome\PhilipsHueApi\Exceptions\UpdateFailedException
     */
    public function updateLight(Light $light): Light
    {
        return $this->lightsRepository->update($light);
    }

    /**
     * @return Room[]
     */
    public function getRooms(): array
    {
        return $this->roomsRepository->getAll();
    }

    /**
     * @param string $id
     * @return Room
     * @throws \IoTHome\PhilipsHueApi\Exceptions\RoomNotFoundException
     */
    public function getRoomById(string $id): Room
    {
        return $this->roomsRepository->getById($id);
    }

    /**
     * @return Zone[]
     */
    public function getZones(): array
    {
        return $this->zonesRepository->getAll();
    }

    /**
     * @param string $id
     * @return Zone
     * @throws \IoTHome\PhilipsHueApi\Exceptions\ZoneNotFoundException
     */
    public function getZoneById(string $id): Zone
    {
        return $this->zonesRepository->getById($id);
    }

    /**
     * @return Scene[]
     */
    public function getScenes(): array
    {
        return $this->scenesRepository->getAll();
    }

    /**
     * @param string $id
     * @return Scene
     * @throws \IoTHome\PhilipsHueApi\Exceptions\SceneNotFoundException
     */
    public function getSceneById(string $id): Scene
    {
        return $this->scenesRepository->getById($id);
    }
}
