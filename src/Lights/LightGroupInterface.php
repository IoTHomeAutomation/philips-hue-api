<?php

declare(strict_types=1);

namespace IoTHome\PhilipsHueApi\Lights;

interface LightGroupInterface
{
    /**
     * @return string[]
     */
    public function getLightIds(): array;
}
