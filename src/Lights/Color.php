<?php

declare(strict_types=1);

namespace IoTHome\PhilipsHueApi\Lights;

use IoTHome\PhilipsHueApi\Exceptions\InvalidValueException;

final class Color
{
    public function __construct(
        private float $x,
        private float $y
    ) {
        $this->validateValue('x', $x);
        $this->validateValue('y', $y);
    }

    public function getX(): float
    {
        return $this->x;
    }

    public function setX(float $x): void
    {
        $this->validateValue('x', $x);

        $this->x = $x;
    }

    public function getY(): float
    {
        return $this->y;
    }

    public function setY(float $y): void
    {
        $this->validateValue('y', $y);

        $this->y = $y;
    }

    private function validateValue(string $name, float $value): void
    {
        if ($value < 0 || $value > 1) {
            throw new InvalidValueException(
                $name . ' must be number between 0 and 1'
            );
        }
    }

    public function getRgbColor(): string
    {
        $x = $this->getX();
        $y = $this->getY();

        $z = 1.0 - $x - $y;
        $Y = 1;
        $X = ($Y / $y) * $x;
        $Z = ($Y / $y) * $z;

        $r = $X * 1.656492 - $Y * 0.354851 - $Z * 0.255038;
        $g = -$X * 0.707196 + $Y * 1.655397 + $Z * 0.036152;
        $b = $X * 0.051713 - $Y * 0.121364 + $Z * 1.011530;

        if ($r > $b && $r > $g && $r > 1) {
            // red is too big
            $g = $g / $r;
            $b = $b / $r;
            $r = 1.0;
        } elseif ($g > $b && $g > $r && $g > 1.0) {
            // green is too big
            $r = $r / $g;
            $b = $b / $g;
            $g = 1.0;
        } elseif ($b > $r && $b > $g && $b > 1.0) {
            // blue is too big
            $r = $r / $b;
            $g = $g / $b;
            $b = 1.0;
        }

        // Apply gamma correction
        $r = $r <= 0.0031308 ? 12.92 * $r : (1.0 + 0.055) * pow($r, (1.0 / 2.4)) - 0.055;
        $g = $g <= 0.0031308 ? 12.92 * $g : (1.0 + 0.055) * pow($g, (1.0 / 2.4)) - 0.055;
        $b = $b <= 0.0031308 ? 12.92 * $b : (1.0 + 0.055) * pow($b, (1.0 / 2.4)) - 0.055;

        if ($r > $b && $r > $g) {
            // red is biggest
            if ($r > 1.0) {
                $g = $g / $r;
                $b = $b / $r;
                $r = 1.0;
            }
        } else {
            if ($g > $b && $g > $r) {
                // green is biggest
                if ($g > 1.0) {
                    $r = $r / $g;
                    $b = $b / $g;
                    $g = 1.0;
                }
            } else {
                if ($b > $r && $b > $g) {
                    // blue is biggest
                    if ($b > 1.0) {
                        $r = $r / $b;
                        $g = $g / $b;
                        $b = 1.0;
                    }
                }
            }
        }

        $r = (int)min(max($r * 255, 0), 255);
        $g = (int)min(max($g * 255, 0), 255);
        $b = (int)min(max($b * 255, 0), 255);

        return '#' .
            str_pad(dechex($r), 2, "0", STR_PAD_LEFT) .
            str_pad(dechex($g), 2, "0", STR_PAD_LEFT) .
            str_pad(dechex($b), 2, "0", STR_PAD_LEFT);
    }

    public function setColorFromRGB(int $red, int $green, int $blue): void
    {
        // Each value must be in interval 0 to 255
        $red = min($red, 255);
        $red = max($red, 0);

        $green = min($green, 255);
        $green = max($green, 0);

        $blue = min($blue, 255);
        $blue = max($blue, 0);

        // Normalize the values to 1
        $red = $red / 255;
        $green = $green / 255;
        $blue = $blue / 255;

        // Gamma correction
        $red = ($red > 0.04045) ? pow(($red + 0.055) / (1.0 + 0.055), 2.4) : ($red / 12.92);
        $green = ($green > 0.04045) ? pow(($green + 0.055) / (1.0 + 0.055), 2.4) : ($green / 12.92);
        $blue = ($blue > 0.04045) ? pow(($blue + 0.055) / (1.0 + 0.055), 2.4) : ($blue / 12.92);

        // Convert the RGB values to XYZ using the Wide RGB D65 conversion formula
        $x = $red * 0.664511 + $green * 0.154324 + $blue * 0.162028;
        $y = $red * 0.283881 + $green * 0.668433 + $blue * 0.047685;
        $z = $red * 0.000088 + $green * 0.072310 + $blue * 0.986039;

        // Calculate the xy values from the XYZ values
        if (($x + $y + $z) <= 0) {
            $x = 0;
            $y = 0;
        } else {
            $x = $x / ($x + $y + $z);
            $y = $y / ($x + $y + $z);
        }

        $x = min(max($x, 0), 1);
        $y = min(max($y, 0), 1);

        $this->setX($x);
        $this->setY($y);
    }
}
