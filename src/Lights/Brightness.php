<?php

declare(strict_types=1);

namespace IoTHome\PhilipsHueApi\Lights;

use IoTHome\PhilipsHueApi\Exceptions\InvalidLightData;
use IoTHome\PhilipsHueApi\Exceptions\InvalidValueException;

final class Brightness
{
    public function __construct(
        private readonly float $minBrightness,
        private float $brightness,
    ) {
        // can happen that a light has set the brightness 0.39, but the minimum is 5
        if ($this->brightness < $this->minBrightness) {
            $this->brightness = $this->minBrightness;
        }

        $this->validateValue($this->brightness);

        if ($this->minBrightness < 0) {
            throw new InvalidLightData('Brightness minimum must not be less than zero');
        }
    }

    public function getMinBrightness(): float
    {
        return $this->minBrightness;
    }

    public function getBrightness(): float
    {
        return $this->brightness;
    }

    public function setBrightness(float $brightness): void
    {
        $this->validateValue($brightness);

        $this->brightness = $brightness;
    }

    private function validateValue(float $brightness): void
    {
        if ($brightness > 100) {
            throw new InvalidValueException('Maximal brightness can be 100');
        }

        if ($brightness < $this->getMinBrightness()) {
            throw new InvalidValueException('Brightness under minimum ' . $this->getMinBrightness());
        }
    }
}
