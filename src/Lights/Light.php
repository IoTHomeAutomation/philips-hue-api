<?php

declare(strict_types=1);

namespace IoTHome\PhilipsHueApi\Lights;

use IoTHome\PhilipsHueApi\Exceptions\InvalidLightData;
use IoTHome\PhilipsHueApi\Exceptions\SetUnsupportedPropertyException;

final class Light
{
    private bool $originalOn;
    private ?float $originalX;
    private ?float $originalY;
    private ?int $originalColorTemperature;
    private ?float $originalBrightness;
    private string $originalName;

    public function __construct(
        private readonly string $id,
        private string $name,
        private readonly string $archetype,
        private bool $on,
        private readonly bool $supportsColors,
        private readonly ?Color $color,
        private readonly bool $supportsColorTemperature,
        private readonly ?ColorTemperature $colorTemperature,
        private readonly bool $supportsBrightness,
        private readonly ?Brightness $brightness
    ) {
        if ($this->supportsColors && $this->color === null) {
            throw new InvalidLightData('Color object must be set when Light supports colors');
        }

        if ($this->supportsColorTemperature && $this->colorTemperature === null) {
            throw new InvalidLightData('ColorTemperature object must be set when Light supports color temperature');
        }

        if ($this->supportsBrightness && $this->brightness === null) {
            throw new InvalidLightData('Brightness object must be set when Light supports brightness settings');
        }

        $this->originalOn = $this->on;
        $this->originalX = $this->color?->getX();
        $this->originalY = $this->color?->getY();
        $this->originalColorTemperature = $this->colorTemperature?->getColorTemperature();
        $this->originalBrightness = $this->brightness?->getBrightness();
        $this->originalName = $this->name;
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getArchetype(): string
    {
        return $this->archetype;
    }

    public function setName(string $name): void
    {
        $this->name = $name;
    }

    public function isOn(): bool
    {
        return $this->on;
    }

    public function isSupportsColors(): bool
    {
        return $this->supportsColors;
    }

    public function getX(): ?float
    {
        return $this->color?->getX();
    }

    public function getY(): ?float
    {
        return $this->color?->getY();
    }

    public function isSupportsColorTemperature(): bool
    {
        return $this->supportsColorTemperature;
    }

    public function getMinColorTemperature(): ?int
    {
        return $this->colorTemperature?->getMinColorTemperature();
    }

    public function getMaxColorTemperature(): ?int
    {
        return $this->colorTemperature?->getMaxColorTemperature();
    }

    public function getColorTemperature(): ?int
    {
        return $this->colorTemperature?->getColorTemperature();
    }

    public function isSupportsBrightness(): bool
    {
        return $this->supportsBrightness;
    }

    public function getMinBrightness(): ?float
    {
        return $this->brightness?->getMinBrightness();
    }

    public function getBrightness(): ?float
    {
        return $this->brightness?->getBrightness();
    }

    public function setOn(bool $on): void
    {
        $this->on = $on;
    }

    public function setX(float $x): void
    {
        if (!$this->supportsColors) {
            throw new SetUnsupportedPropertyException(
                'Try to set color on a light that does not support color settings'
            );
        }

        $this->color?->setX($x);
    }

    public function setY(float $y): void
    {
        if (!$this->supportsColors) {
            throw new SetUnsupportedPropertyException(
                'Try to set color on a light that does not support color settings'
            );
        }

        $this->color?->setY($y);
    }

    public function setColorTemperature(?int $colorTemperature): void
    {
        if (!$this->supportsColorTemperature) {
            throw new SetUnsupportedPropertyException(
                'Try to set color temperature on a light that does not support color settings'
            );
        }

        $this->colorTemperature?->setColorTemperature($colorTemperature);
    }

    public function setBrightness(float $brightness): void
    {
        if (!$this->supportsBrightness) {
            throw new SetUnsupportedPropertyException(
                'Try to set brightness on a light that does not support brightness settings'
            );
        }

        $this->brightness?->setBrightness($brightness);
    }

    /**
     * @return array<string, bool>
     */
    public function getChangedAttributes(): array
    {
        $data = [];

        if ($this->on !== $this->originalOn) {
            $data['on'] = true;
        }
        if ($this->color?->getX() !== $this->originalX) {
            $data['x'] = true;
        }
        if ($this->color?->getY() !== $this->originalY) {
            $data['y'] = true;
        }
        if ($this->colorTemperature?->getColorTemperature() !== $this->originalColorTemperature) {
            $data['colorTemperature'] = true;
        }
        if ($this->brightness?->getBrightness() !== $this->originalBrightness) {
            $data['brightness'] = true;
        }
        if ($this->name !== $this->originalName) {
            $data['name'] = true;
        }

        return $data;
    }

    public function setColorFromRGB(int $red, int $green, int $blue): void
    {
        if (!$this->supportsColors) {
            throw new SetUnsupportedPropertyException(
                'Try to set color on a light that does not support color settings'
            );
        }

        $this->color?->setColorFromRGB($red, $green, $blue);
    }

    public function getRgbColor(): ?string
    {
        return $this->color?->getRgbColor();
    }
}
