<?php

declare(strict_types=1);

namespace IoTHome\PhilipsHueApi\Lights;

use IoTHome\PhilipsHueApi\Exceptions\InvalidLightData;
use IoTHome\PhilipsHueApi\Exceptions\InvalidValueException;

final class ColorTemperature
{
    public function __construct(
        private readonly int $minColorTemperature,
        private readonly int $maxColorTemperature,
        private ?int $colorTemperature,
    ) {
        if ($this->minColorTemperature < 0) {
            throw new InvalidLightData('Minimum color temperature must not be less than zero');
        }

        if ($this->maxColorTemperature <= $this->minColorTemperature) {
            throw new InvalidLightData('Maximum color temperature must be greater than minimum');
        }
    }

    public function getMinColorTemperature(): int
    {
        return $this->minColorTemperature;
    }

    public function getMaxColorTemperature(): int
    {
        return $this->maxColorTemperature;
    }

    public function getColorTemperature(): ?int
    {
        return $this->colorTemperature;
    }

    public function setColorTemperature(?int $colorTemperature): void
    {
        if ($colorTemperature !== null && $colorTemperature > $this->getMaxColorTemperature()) {
            throw new InvalidValueException('Color temperature above maximum ' . $this->getMaxColorTemperature());
        }

        if ($colorTemperature !== null && $colorTemperature < $this->getMinColorTemperature()) {
            throw new InvalidValueException('Color temperature under minimum ' . $this->getMinColorTemperature());
        }

        $this->colorTemperature = $colorTemperature;
    }
}
