<?php

declare(strict_types=1);

namespace IoTHome\PhilipsHueApi\Lights;

use IoTHome\PhilipsHueApi\Client\APIClientInterface;
use IoTHome\PhilipsHueApi\Exceptions\LightNotFoundException;
use IoTHome\PhilipsHueApi\Exceptions\UpdateFailedException;

final class LightsApiRepository implements LightsRepositoryInterface
{
    public function __construct(
        private readonly APIClientInterface $apiClient
    ) {
    }

    /**
     * @return Light[]
     */
    public function getAll(): array
    {
        $response = $this->apiClient->readResource('light');

        if ($response->isSuccess()) {
            $lights = [];

            $factory = new LightFactory();

            $data = $response->getData();

            foreach ($data as $lightData) {
                if (is_array($lightData)) {
                    $lights[] = $factory->createFromApiData($lightData);
                }
            }

            return $lights;
        } else {
            return [];
        }
    }

    /**
     * @param string $id
     * @return Light
     * @throws LightNotFoundException
     */
    public function getById(string $id): Light
    {
        $response = $this->apiClient->readResource('light', $id);

        if ($response->isSuccess()) {
            $factory = new LightFactory();

            $data = $response->getData();
            $data = $data[0] ?? [];

            if (is_array($data)) {
                return $factory->createFromApiData($data);
            }
        }

        throw new LightNotFoundException();
    }

    /**
     * @param Light $light
     * @return Light
     * @throws LightNotFoundException
     * @throws UpdateFailedException
     */
    public function update(Light $light): Light
    {
        $data = [];
        $changedAttributes = $light->getChangedAttributes();

        foreach (array_keys($changedAttributes) as $changedAttribute) {
            switch ($changedAttribute) {
                case 'on':
                    $data['on']['on'] = $light->isOn();
                    break;
                case 'brightness':
                    $data['dimming']['brightness'] = $light->getBrightness();
                    break;
                case 'colorTemperature':
                    $data['color_temperature']['mirek'] = $light->getColorTemperature();
                    break;
                case 'x':
                    $data['color']['xy']['x'] = $light->getX();
                    break;
                case 'y':
                    $data['color']['xy']['y'] = $light->getY();
                    break;
                case 'name':
                    $data['metadata']['name'] = $light->getName();
                    break;
            }
        }

        if (!empty($data)) {
            $response = $this->apiClient->updateResource('light', $light->getId(), $data);

            if (!$response->isSuccess()) {
                $info = print_r($response->getError(), true);
                throw new UpdateFailedException('Update request failed. Error: ' . $info);
            }
        }

        return $this->getById($light->getId());
    }
}
