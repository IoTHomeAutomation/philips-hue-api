<?php

declare(strict_types=1);

namespace IoTHome\PhilipsHueApi\Lights;

use IoTHome\PhilipsHueApi\Exceptions\LightNotFoundException;
use IoTHome\PhilipsHueApi\Exceptions\UpdateFailedException;

interface LightsRepositoryInterface
{
    /**
     * @return Light[]
     */
    public function getAll(): array;

    /**
     * @param string $id
     * @return Light
     * @throws LightNotFoundException
     */
    public function getById(string $id): Light;

    /**
     * @param Light $light
     * @return Light
     * @throws LightNotFoundException
     * @throws UpdateFailedException
     */
    public function update(Light $light): Light;
}
