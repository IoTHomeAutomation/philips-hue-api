<?php

declare(strict_types=1);

namespace IoTHome\PhilipsHueApi\Lights;

final class LightFactory
{
    /**
     * @param array<mixed> $data
     * @return Light
     */
    public function createFromApiData(array $data): Light
    {
        $supportsColor = isset($data['color']);
        $color = null;

        if ($supportsColor) {
            $color = new Color(
                isset($data['color']['xy']['x']) ? (float)$data['color']['xy']['x'] : 0,
                isset($data['color']['xy']['y']) ? (float)$data['color']['xy']['y'] : 0
            );
        }

        $supportsColorTemperature = isset($data['color_temperature']['mirek_schema']);
        $colorTemperature = null;

        if ($supportsColorTemperature) {
            $colorTemperature = new ColorTemperature(
                isset($data['color_temperature']['mirek_schema']['mirek_minimum']) ?
                    (int)$data['color_temperature']['mirek_schema']['mirek_minimum'] : 0,
                isset($data['color_temperature']['mirek_schema']['mirek_maximum']) ?
                    (int)$data['color_temperature']['mirek_schema']['mirek_maximum'] : 0,
                isset($data['color_temperature']['mirek']) ?
                    (int)$data['color_temperature']['mirek'] : null
            );
        }

        $supportsBrightness = isset($data['dimming']);
        $brightness = null;

        if ($supportsBrightness) {
            $brightness = new Brightness(
                isset($data['dimming']['min_dim_level']) ? (float)$data['dimming']['min_dim_level'] : 0,
                isset($data['dimming']['brightness']) ? (float)$data['dimming']['brightness'] : 0
            );
        }

        return new Light(
            (string)($data['id'] ?? ''),
            (string)($data['metadata']['name'] ?? ''),
            (string)($data['metadata']['archetype'] ?? ''),
            (bool)($data['on']['on'] ?? false),
            $supportsColor,
            $color,
            $supportsColorTemperature,
            $colorTemperature,
            $supportsBrightness,
            $brightness
        );
    }
}
