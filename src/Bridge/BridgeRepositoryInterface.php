<?php

declare(strict_types=1);

namespace IoTHome\PhilipsHueApi\Bridge;

use IoTHome\PhilipsHueApi\Exceptions\BridgeUnavailableException;
use IoTHome\PhilipsHueApi\Exceptions\LinkButtonNotPressedException;
use IoTHome\PhilipsHueApi\Exceptions\UnableToGetKeyException;

interface BridgeRepositoryInterface
{
    /**
     * @throws BridgeUnavailableException
     * @throws LinkButtonNotPressedException
     * @throws UnableToGetKeyException
     */
    public function generateKey(string $applicationName): string;

    public function isConnectedAndAuthenticated(): bool;
}
