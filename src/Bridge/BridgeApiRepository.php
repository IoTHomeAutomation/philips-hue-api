<?php

declare(strict_types=1);

namespace IoTHome\PhilipsHueApi\Bridge;

use IoTHome\PhilipsHueApi\Client\APIClientInterface;
use IoTHome\PhilipsHueApi\Exceptions\BridgeUnavailableException;
use IoTHome\PhilipsHueApi\Exceptions\LinkButtonNotPressedException;
use IoTHome\PhilipsHueApi\Exceptions\UnableToGetKeyException;

final class BridgeApiRepository implements BridgeRepositoryInterface
{
    public function __construct(
        private readonly APIClientInterface $apiClient
    ) {
    }

    /**
     * @throws BridgeUnavailableException
     * @throws LinkButtonNotPressedException
     * @throws UnableToGetKeyException
     */
    public function generateKey(string $applicationName): string
    {
        $data = [
            'devicetype' => $applicationName,
            'generateclientkey' => true,
        ];

        $response = $this->apiClient->callAPIDirectly('api', 'POST', $data, false);

        if ($response->isSuccess()) {
            $error = $response->getError();

            if (!empty($error)) {
                if (isset($error['type']) && (int)$error['type'] === 101) {
                    throw new LinkButtonNotPressedException();
                } else {
                    $message = isset($error['description']) && is_string(
                        $error['description']
                    ) ? $error['description'] : '';
                    throw new UnableToGetKeyException($message);
                }
            }

            $data = $response->getData();
            if (!empty($data) && isset($data['username']) && is_string($data['username'])) {
                return $data['username'];
            }
        } else {
            throw new BridgeUnavailableException();
        }

        throw new UnableToGetKeyException();
    }

    public function isConnectedAndAuthenticated(): bool
    {
        $response = $this->apiClient->readResource('bridge', null, true);

        return $response->isSuccess();
    }
}
