<?php

declare(strict_types=1);

namespace IoTHome\PhilipsHueApi\Exceptions;

abstract class ConnectionException extends \Exception
{
}
