<?php

declare(strict_types=1);

namespace IoTHome\PhilipsHueApi\Exceptions;

abstract class HueLogicException extends \LogicException
{
}
