<?php

declare(strict_types=1);

namespace IoTHome\PhilipsHueApi\Exceptions;

final class RoomNotFoundException extends BridgeException
{
}
