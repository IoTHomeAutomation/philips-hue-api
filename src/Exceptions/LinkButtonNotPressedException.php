<?php

declare(strict_types=1);

namespace IoTHome\PhilipsHueApi\Exceptions;

final class LinkButtonNotPressedException extends BridgeException
{
}
