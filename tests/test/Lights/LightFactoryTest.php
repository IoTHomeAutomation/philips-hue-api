<?php

declare(strict_types=1);

namespace IoTHome\Tests\PhilipsHueApi\Lights;

use IoTHome\PhilipsHueApi\Lights\LightFactory;
use PHPUnit\Framework\TestCase;

final class LightFactoryTest extends TestCase
{
    /**
     * @test
     */
    public function itCreatesLight(): void
    {
        $data = [
            'id' => 'abcd',
            'metadata' => [
                'name' => 'test',
                'archetype' => 'light',
            ],
            'on' => [
                'on' => true,
            ],
            'dimming' => [
                'brightness' => 50,
                'min_dim_level' => 0.5,
            ],
            'color_temperature' => [
                'mirek' => 250,
                'mirek_schema' => [
                    'mirek_minimum' => 120,
                    'mirek_maximum' => 500,
                ]
            ],
            'color' => [
                'xy' => [
                    'x' => 0,
                    'y' => 0.2,
                ]
            ]
        ];

        $factory = new LightFactory();
        $light = $factory->createFromApiData($data);

        $this->assertEquals('abcd', $light->getId());
        $this->assertEquals('test', $light->getName());
        $this->assertEquals('light', $light->getArchetype());
        $this->assertEquals(true, $light->isOn());

        $this->assertEquals(true, $light->isSupportsBrightness());
        $this->assertEquals(50, $light->getBrightness());
        $this->assertEquals(0.5, $light->getMinBrightness());

        $this->assertEquals(true, $light->isSupportsColors());
        $this->assertEquals(0, $light->getX());
        $this->assertEquals(0.2, $light->getY());

        $this->assertEquals(true, $light->isSupportsColorTemperature());
        $this->assertEquals(250, $light->getColorTemperature());
        $this->assertEquals(500, $light->getMaxColorTemperature());
        $this->assertEquals(120, $light->getMinColorTemperature());
    }

    /**
     * @test
     */
    public function itCreatesLightWithoutColors(): void
    {
        $data = [
            'id' => 'abcd',
            'metadata' => [
                'name' => 'test',
                'archetype' => 'light',
            ],
            'on' => [
                'on' => true,
            ],
            'dimming' => [
                'brightness' => 50,
                'min_dim_level' => 0.5,
            ],
        ];

        $factory = new LightFactory();
        $light = $factory->createFromApiData($data);

        $this->assertEquals('abcd', $light->getId());
        $this->assertEquals('test', $light->getName());
        $this->assertEquals('light', $light->getArchetype());
        $this->assertEquals(true, $light->isOn());

        $this->assertEquals(true, $light->isSupportsBrightness());
        $this->assertEquals(50, $light->getBrightness());
        $this->assertEquals(0.5, $light->getMinBrightness());

        $this->assertEquals(false, $light->isSupportsColors());
        $this->assertNull($light->getX());
        $this->assertNull($light->getY());

        $this->assertEquals(false, $light->isSupportsColorTemperature());
        $this->assertNull($light->getColorTemperature());
        $this->assertNull($light->getMaxColorTemperature());
        $this->assertNull($light->getMinColorTemperature());
    }

    /**
     * @test
     */
    public function itCreatesLightWithoutColorsAndBrightnes(): void
    {
        $data = [
            'id' => 'abcd',
            'metadata' => [
                'name' => 'test',
                'archetype' => 'light',
            ],
            'on' => [
                'on' => true,
            ],
        ];

        $factory = new LightFactory();
        $light = $factory->createFromApiData($data);

        $this->assertEquals('abcd', $light->getId());
        $this->assertEquals('test', $light->getName());
        $this->assertEquals('light', $light->getArchetype());
        $this->assertEquals(true, $light->isOn());

        $this->assertEquals(false, $light->isSupportsBrightness());
        $this->assertNull($light->getBrightness());
        $this->assertNull($light->getMinBrightness());

        $this->assertEquals(false, $light->isSupportsColors());
        $this->assertNull($light->getX());
        $this->assertNull($light->getY());

        $this->assertEquals(false, $light->isSupportsColorTemperature());
        $this->assertNull($light->getColorTemperature());
        $this->assertNull($light->getMaxColorTemperature());
        $this->assertNull($light->getMinColorTemperature());
    }
}
