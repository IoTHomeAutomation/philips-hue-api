<?php

declare(strict_types=1);

namespace IoTHome\Tests\PhilipsHueApi\Lights;

use IoTHome\PhilipsHueApi\Exceptions\InvalidValueException;
use IoTHome\PhilipsHueApi\Lights\Color;
use PHPUnit\Framework\TestCase;

final class ColorTest extends TestCase
{
    /**
     * @test
     */
    public function itThrowExceptionWhenInvalidXValueIsSet(): void
    {
        $color = new Color(0, 0);

        $this->expectException(InvalidValueException::class);
        $color->setX(2);
    }

    /**
     * @test
     */
    public function itThrowExceptionWhenInvalidXValueIsSet2(): void
    {
        $color = new Color(0, 0);

        $this->expectException(InvalidValueException::class);
        $color->setX(-1);
    }

    /**
     * @test
     */
    public function itThrowExceptionWhenInvalidXValueIsInicialized(): void
    {
        $this->expectException(InvalidValueException::class);
        new Color(2, 0);
    }

    /**
     * @test
     */
    public function itThrowExceptionWhenInvalidZValueIsSet(): void
    {
        $color = new Color(0, 0);

        $this->expectException(InvalidValueException::class);
        $color->setY(2);
    }

    /**
     * @test
     */
    public function itThrowExceptionWhenInvalidZValueIsSet2(): void
    {
        $color = new Color(0, 0);

        $this->expectException(InvalidValueException::class);
        $color->setY(-1);
    }

    /**
     * @test
     */
    public function itThrowExceptionWhenInvalidYValueIsInicialized(): void
    {
        $this->expectException(InvalidValueException::class);
        new Color(0, 2);
    }

    /**
     * @test
     */
    public function itSetColorFromRGB(): void
    {
        $color = new Color(0, 0);
        // #0000FF
        $color->setColorFromRGB(0, 0, 255);

        $this->assertEquals(0.1355030140, round($color->getX(), 10));
        $this->assertEquals(0.0407833547, round($color->getY(), 10));
        // because of corrections the color is not same but similar
        $this->assertEquals('#0005fe', $color->getRgbColor());
    }
}
