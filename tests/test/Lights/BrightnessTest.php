<?php

declare(strict_types=1);

namespace IoTHome\Tests\PhilipsHueApi\Lights;

use IoTHome\PhilipsHueApi\Exceptions\InvalidLightData;
use IoTHome\PhilipsHueApi\Exceptions\InvalidValueException;
use IoTHome\PhilipsHueApi\Lights\Brightness;
use PHPUnit\Framework\TestCase;

final class BrightnessTest extends TestCase
{
    /**
     * @test
     */
    public function itThrowExceptionWhenMaximumIsExceeded(): void
    {
        $brightness = new Brightness(0.2, 10);

        $this->expectException(InvalidValueException::class);
        $brightness->setBrightness(101);
    }

    /**
     * @test
     */
    public function itThrowExceptionWhenMinimumIsLessThenZero(): void
    {
        $this->expectException(InvalidLightData::class);
        new Brightness(-1, 10);
    }

    /**
     * @test
     */
    public function itThrowExceptionWhenMinimumIsExceeded(): void
    {
        $brightness = new Brightness(0.2, 10);

        $this->expectException(InvalidValueException::class);
        $brightness->setBrightness(0.1);
    }

    /**
     * @test
     */
    public function itStoreValidValue(): void
    {
        $brightness = new Brightness(0.2, 10);

        $brightness->setBrightness(1);
        $this->assertEquals(1, $brightness->getBrightness());

        $brightness->setBrightness(100);
        $this->assertEquals(100, $brightness->getBrightness());

        $brightness->setBrightness(0.2);
        $this->assertEquals(0.2, $brightness->getBrightness());
    }
}
