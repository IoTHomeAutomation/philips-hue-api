<?php

declare(strict_types=1);

namespace IoTHome\Tests\PhilipsHueApi\Lights;

use IoTHome\PhilipsHueApi\Exceptions\InvalidLightData;
use IoTHome\PhilipsHueApi\Exceptions\InvalidValueException;
use IoTHome\PhilipsHueApi\Lights\ColorTemperature;
use PHPUnit\Framework\TestCase;

final class ColorTemperatureTest extends TestCase
{
    /**
     * @test
     */
    public function itSetsValidColorTemperature(): void
    {
        $colorTemperature = new ColorTemperature(0, 100, 50);

        $colorTemperature->setColorTemperature(20);
        $this->assertEquals(20, $colorTemperature->getColorTemperature());

        $colorTemperature->setColorTemperature(0);
        $this->assertEquals(0, $colorTemperature->getColorTemperature());

        $colorTemperature->setColorTemperature(100);
        $this->assertEquals(100, $colorTemperature->getColorTemperature());
    }

    /**
     * @test
     */
    public function itThrowExceptionWhenMaximumIsExceeded(): void
    {
        $colorTemperature = new ColorTemperature(0, 100, 50);

        $this->expectException(InvalidValueException::class);
        $colorTemperature->setColorTemperature(101);
    }

    /**
     * @test
     */
    public function itThrowExceptionWhenValueIsLessThenMinimum(): void
    {
        $colorTemperature = new ColorTemperature(1, 100, 50);

        $this->expectException(InvalidValueException::class);
        $colorTemperature->setColorTemperature(0);
    }

    /**
     * @test
     */
    public function itThrowExceptionWhenMinimumIsLessThenZero(): void
    {
        $this->expectException(InvalidLightData::class);
        new ColorTemperature(-1, 100, 50);
    }

    /**
     * @test
     */
    public function itSetsNull(): void
    {
        $colorTemperature = new ColorTemperature(1, 100, 50);

        $colorTemperature->setColorTemperature(null);
        $this->assertNull($colorTemperature->getColorTemperature());
    }

    /**
     * @test
     */
    public function itThrowExceptionWhenMaximumIsLessThenMinimum(): void
    {
        $this->expectException(InvalidLightData::class);
        new ColorTemperature(10, 9, 50);
    }
}
