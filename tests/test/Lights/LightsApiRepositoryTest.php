<?php

declare(strict_types=1);

namespace IoTHome\Tests\PhilipsHueApi\Lights;

use IoTHome\PhilipsHueApi\Client\APIClientInterface;
use IoTHome\PhilipsHueApi\Lights\Light;
use IoTHome\PhilipsHueApi\Lights\LightsApiRepository;
use IoTHome\Tests\PhilipsHueApi\Client\DummyAPIClient;
use PHPUnit\Framework\TestCase;

final class LightsApiRepositoryTest extends TestCase
{
    public const TEST_LIGHT_ID = 'abcdef12-abcd-1234-95d1-fb6fcb804d4c';

    private function getAPIClient(): APIClientInterface
    {
        return new DummyAPIClient();
    }

    /**
     * @test
     */
    public function itGetsAll(): void
    {
        $repository = new LightsApiRepository($this->getAPIClient());

        $collection = $repository->getAll();

        $this->assertEquals(6, count($collection));

        $first = reset($collection);

        $this->assertInstanceOf(Light::class, $first);

        if ($first instanceof Light) {
            $this->assertEquals('abcdef12-abcd-1234-a48c-7a009442db42', $first->getId());
            $this->assertEquals('Light hall', $first->getName());

            $this->assertFalse($first->isOn());

            $this->assertTrue($first->isSupportsBrightness());
            $this->assertEquals(77.56, $first->getBrightness());
            $this->assertEquals(5.0, $first->getMinBrightness());

            $this->assertFalse($first->isSupportsColors());
            $this->assertNull($first->getX());
            $this->assertNull($first->getY());

            $this->assertFalse($first->isSupportsColorTemperature());
        }
    }

    /**
     * @test
     */
    public function itGetsById(): void
    {
        $repository = new LightsApiRepository($this->getAPIClient());

        $light = $repository->getById(self::TEST_LIGHT_ID);

        $this->assertEquals(self::TEST_LIGHT_ID, $light->getId());
        $this->assertEquals('Light living room', $light->getName());

        $this->assertFalse($light->isOn());

        $this->assertTrue($light->isSupportsBrightness());
        $this->assertEquals(56.69, $light->getBrightness());
        $this->assertEquals(0.20000000298023224, $light->getMinBrightness());

        $this->assertTrue($light->isSupportsColors());
        $this->assertEquals(0.5016, $light->getX());
        $this->assertEquals(0.4151, $light->getY());

        $this->assertTrue($light->isSupportsColorTemperature());
        $this->assertEquals(443, $light->getColorTemperature());
        $this->assertEquals(500, $light->getMaxColorTemperature());
        $this->assertEquals(153, $light->getMinColorTemperature());
    }

    /**
     * @test
     */
    public function itUpdatesLightState(): void
    {
        $repository = new LightsApiRepository($this->getAPIClient());

        $light = $repository->getById(self::TEST_LIGHT_ID);

        $this->assertFalse($light->isOn());
        $light->setOn(true);

        $repository->update($light);

        $updatedLight = $repository->getById(self::TEST_LIGHT_ID);

        $this->assertLightsEqual($light, $updatedLight);
    }

    /**
     * @test
     */
    public function itUpdatesLightBrightness(): void
    {
        $repository = new LightsApiRepository($this->getAPIClient());

        $light = $repository->getById(self::TEST_LIGHT_ID);

        $this->assertEquals(56.69, $light->getBrightness());
        $light->setBrightness(66);

        $repository->update($light);

        $updatedLight = $repository->getById(self::TEST_LIGHT_ID);

        $this->assertLightsEqual($light, $updatedLight);
    }

    /**
     * @test
     */
    public function itUpdatesLightColor(): void
    {
        $repository = new LightsApiRepository($this->getAPIClient());

        $light = $repository->getById(self::TEST_LIGHT_ID);

        $this->assertEquals(0.5016, $light->getX());
        $this->assertEquals(0.4151, $light->getY());
        $light->setX(0.5);
        $light->setY(0.7);

        $repository->update($light);

        $updatedLight = $repository->getById(self::TEST_LIGHT_ID);

        $this->assertLightsEqual($light, $updatedLight);
    }

    /**
     * @test
     */
    public function itUpdatesLightColorTemperature(): void
    {
        $repository = new LightsApiRepository($this->getAPIClient());

        $light = $repository->getById(self::TEST_LIGHT_ID);

        $this->assertEquals(443, $light->getColorTemperature());
        $light->setColorTemperature(250);

        $repository->update($light);

        $updatedLight = $repository->getById(self::TEST_LIGHT_ID);

        $this->assertLightsEqual($light, $updatedLight);
    }

    private function assertLightsEqual(Light $first, Light $second): void
    {
        $this->assertEquals($first->getId(), $second->getId());
        $this->assertEquals($first->getName(), $second->getName());
        $this->assertEquals($first->getArchetype(), $second->getArchetype());
        $this->assertEquals($first->isOn(), $second->isOn());

        $this->assertEquals($first->isSupportsBrightness(), $second->isSupportsBrightness());
        $this->assertEquals($first->getBrightness(), $second->getBrightness());
        $this->assertEquals($first->getMinBrightness(), $second->getMinBrightness());

        $this->assertEquals($first->isSupportsColors(), $second->isSupportsColors());
        $this->assertEquals($first->getX(), $second->getX());
        $this->assertEquals($first->getY(), $second->getY());

        $this->assertEquals($first->isSupportsColorTemperature(), $second->isSupportsColorTemperature());
        $this->assertEquals($first->getColorTemperature(), $second->getColorTemperature());
        $this->assertEquals($first->getMaxColorTemperature(), $second->getMaxColorTemperature());
        $this->assertEquals($first->getMinColorTemperature(), $second->getMinColorTemperature());
    }
}
