<?php

declare(strict_types=1);

namespace IoTHome\Tests\PhilipsHueApi\Lights;

use IoTHome\PhilipsHueApi\Exceptions\InvalidLightData;
use IoTHome\PhilipsHueApi\Exceptions\SetUnsupportedPropertyException;
use IoTHome\PhilipsHueApi\Lights\Brightness;
use IoTHome\PhilipsHueApi\Lights\Color;
use IoTHome\PhilipsHueApi\Lights\ColorTemperature;
use IoTHome\PhilipsHueApi\Lights\Light;
use PHPUnit\Framework\TestCase;

final class LightTest extends TestCase
{
    /**
     * @test
     */
    public function itThrowsExceptionWhenSupportsColorButObjectIsMissing(): void
    {
        $this->expectException(InvalidLightData::class);

        new Light(
            'a',
            'test',
            'light',
            true,
            true,
            null,
            false,
            null,
            false,
            null
        );
    }

    /**
     * @test
     */
    public function itThrowsExceptionWhenSupportsColorTemperatureButObjectIsMissing(): void
    {
        $this->expectException(InvalidLightData::class);

        new Light(
            'a',
            'test',
            'light',
            true,
            false,
            null,
            true,
            null,
            false,
            null
        );
    }

    /**
     * @test
     */
    public function itThrowsExceptionWhenSupportsBrightnessButObjectIsMissing(): void
    {
        $this->expectException(InvalidLightData::class);

        new Light(
            'a',
            'test',
            'light',
            true,
            false,
            null,
            false,
            null,
            true,
            null
        );
    }

    /**
     * @test
     */
    public function itReturnsChangedAttributes(): void
    {
        $light = new Light(
            'a',
            'test',
            'light',
            true,
            true,
            new Color(1, 1),
            true,
            new ColorTemperature(1, 10, 5),
            true,
            new Brightness(0.5, 10)
        );

        $light->setX(0);
        $light->setY(0);
        $light->setColorTemperature(10);
        $light->setBrightness(15);
        $light->setOn(false);
        $light->setName('b');

        $changedAttributes = $light->getChangedAttributes();

        $expected = [
            'on' => true,
            'name' => true,
            'x' => true,
            'y' => true,
            'brightness' => true,
            'colorTemperature' => true
        ];

        $this->assertEqualsCanonicalizing($expected, $changedAttributes);
    }

    /**
     * @test
     */
    public function itReturnsEmptyChangedAttributesAfterSetSameValue(): void
    {
        $light = new Light(
            'a',
            'test',
            'light',
            true,
            true,
            new Color(1, 1),
            true,
            new ColorTemperature(1, 10, 5),
            true,
            new Brightness(0.5, 10)
        );

        $light->setX(0);
        $light->setX(1);
        $light->setY(0);
        $light->setY(1);
        $light->setColorTemperature(10);
        $light->setColorTemperature(5);
        $light->setBrightness(15);
        $light->setBrightness(10);
        $light->setOn(false);
        $light->setOn(true);
        $light->setName('tset');
        $light->setName('test');

        $changedAttributes = $light->getChangedAttributes();

        $this->assertEmpty($changedAttributes);
    }

    /**
     * @test
     */
    public function itThrowExceptionWhenColorXIsSetWithoutSupport(): void
    {
        $light = new Light(
            'a',
            'test',
            'light',
            true,
            false,
            null,
            false,
            null,
            false,
            null
        );

        $this->expectException(SetUnsupportedPropertyException::class);
        $light->setX(1);
    }

    /**
     * @test
     */
    public function itThrowExceptionWhenColorYIsSetWithoutSupport(): void
    {
        $light = new Light(
            'a',
            'test',
            'light',
            true,
            false,
            null,
            false,
            null,
            false,
            null
        );

        $this->expectException(SetUnsupportedPropertyException::class);
        $light->setY(1);
    }

    /**
     * @test
     */
    public function itThrowExceptionWhenColorTemperatureIsSetWithoutSupport(): void
    {
        $light = new Light(
            'a',
            'test',
            'light',
            true,
            false,
            null,
            false,
            null,
            false,
            null
        );

        $this->expectException(SetUnsupportedPropertyException::class);
        $light->setColorTemperature(1);
    }

    /**
     * @test
     */
    public function itThrowExceptionWhenBrightnessIsSetWithoutSupport(): void
    {
        $light = new Light(
            'a',
            'test',
            'light',
            true,
            false,
            null,
            false,
            null,
            false,
            null
        );

        $this->expectException(SetUnsupportedPropertyException::class);
        $light->setBrightness(1);
    }
}
