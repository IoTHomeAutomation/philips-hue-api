<?php

declare(strict_types=1);

namespace IoTHome\Tests\PhilipsHueApi\Zones;

use IoTHome\PhilipsHueApi\Client\APIClientInterface;
use IoTHome\PhilipsHueApi\Zones\Zone;
use IoTHome\PhilipsHueApi\Zones\ZonesApiRepository;
use IoTHome\Tests\PhilipsHueApi\Client\DummyAPIClient;
use PHPUnit\Framework\TestCase;

final class ZonesApiRepositoryTest extends TestCase
{
    public const TEST_ZONE_ID = 'abcdef12-abcd-1234-bf7b-ecf6d26c9d94';

    private function getAPIClient(): APIClientInterface
    {
        return new DummyAPIClient();
    }

    /**
     * @test
     */
    public function itGetsAll(): void
    {
        $repository = new ZonesApiRepository($this->getAPIClient());

        $collection = $repository->getAll();

        $this->assertEquals(1, count($collection));

        $first = reset($collection);

        $this->assertInstanceOf(Zone::class, $first);

        if ($first instanceof Zone) {
            $this->assertEquals('abcdef12-abcd-1234-bf7b-ecf6d26c9d94', $first->getId());
            $this->assertEquals('TV Zone', $first->getName());
            $this->assertEquals('tv', $first->getArchetype());
            $this->assertEquals(
                [
                    'abcdef12-abcd-1234-ab9a-30659512b31f',
                    'abcdef12-abcd-1234-982b-ae15497773de',
                    'abcdef12-abcd-1234-a48c-7a009442db42',
                    'abcdef12-abcd-1234-ab9c-497d5e8b9512',
                ],
                $first->getLightIds()
            );
        }
    }

    /**
     * @test
     */
    public function itGetsById(): void
    {
        $repository = new ZonesApiRepository($this->getAPIClient());

        $zone = $repository->getById(self::TEST_ZONE_ID);

        $this->assertInstanceOf(Zone::class, $zone);
        $this->assertEquals('abcdef12-abcd-1234-bf7b-ecf6d26c9d94', $zone->getId());
        $this->assertEquals('TV Zone', $zone->getName());
        $this->assertEquals('tv', $zone->getArchetype());
        $this->assertEquals(
            [
                'abcdef12-abcd-1234-ab9a-30659512b31f',
                'abcdef12-abcd-1234-982b-ae15497773de',
                'abcdef12-abcd-1234-a48c-7a009442db42',
                'abcdef12-abcd-1234-ab9c-497d5e8b9512',
            ],
            $zone->getLightIds()
        );
    }
}
