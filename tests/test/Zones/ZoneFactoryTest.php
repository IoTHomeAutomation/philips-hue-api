<?php

declare(strict_types=1);

namespace IoTHome\Tests\PhilipsHueApi\Zones;

use IoTHome\PhilipsHueApi\Zones\Zone;
use IoTHome\PhilipsHueApi\Zones\ZoneFactory;
use PHPUnit\Framework\TestCase;

final class ZoneFactoryTest extends TestCase
{
    /**
     * @test
     */
    public function itCreatesZone(): void
    {
        $data = [
            'id' => 'abcd',
            'metadata' => [
                'name' => 'test',
                'archetype' => 'tv',
            ],
            'services' => [
                [
                    'rid' => 'a1',
                    'rtype' => 'light',
                ],
                [
                    'rid' => 'a2',
                    'rtype' => 'light',
                ],
                [
                    'rid' => 'b1',
                    'rtype' => 'grouped_light',
                ],
            ]
        ];

        $factory = new ZoneFactory();
        $zone = $factory->createFromApiData($data);

        $this->assertInstanceOf(Zone::class, $zone);
        $this->assertEquals('abcd', $zone->getId());
        $this->assertEquals('test', $zone->getName());
        $this->assertEquals('tv', $zone->getArchetype());
        $this->assertEquals(['a1', 'a2'], $zone->getLightIds());
    }
}
