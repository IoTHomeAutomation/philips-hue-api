<?php

declare(strict_types=1);

namespace IoTHome\Tests\PhilipsHueApi\Client;

use IoTHome\PhilipsHueApi\Client\APIClientInterface;
use IoTHome\PhilipsHueApi\Client\APIResponse;
use IoTHome\Tests\PhilipsHueApi\Lights\LightsApiRepositoryTest;
use IoTHome\Tests\PhilipsHueApi\Rooms\RoomsApiRepositoryTest;
use IoTHome\Tests\PhilipsHueApi\Scenes\ScenesApiRepositoryTest;
use IoTHome\Tests\PhilipsHueApi\Zones\ZonesApiRepositoryTest;

final class DummyAPIClient implements APIClientInterface
{
    /** @var array<mixed> */
    private array $lights;

    /** @var array<mixed> */
    private array $light;

    /**
     */
    public function __construct()
    {
        $lightJson = file_get_contents(__DIR__ . '/light.json') ?: '';
        $this->lights = (array)(json_decode($lightJson, true) ?: []);

        $lightByIdJson = file_get_contents(__DIR__ . '/light-by-id.json') ?: '';
        $this->light = (array)(json_decode($lightByIdJson, true) ?: []);
    }


    public function createResource(string $resource, array $data, bool $authorise = true): APIResponse
    {
        return new APIResponse(false, [], []);
    }

    public function readResource(string $resource, ?string $id = null, bool $authorise = true): APIResponse
    {
        // --------------------------------------------------
        // ------------------- Lights -----------------------
        // --------------------------------------------------
        if ($resource === 'light' && !$id && $authorise) {
            $data = isset($this->lights['data']) && is_array($this->lights['data']) ? $this->lights['data'] : [];
            $error = isset($this->lights['error']) && is_array($this->lights['error']) ? $this->lights['error'] : [];
            return new APIResponse(true, $data, $error);
        }

        if ($resource === 'light' && $id === LightsApiRepositoryTest::TEST_LIGHT_ID && $authorise) {
            $data = isset($this->light['data']) && is_array($this->light['data']) ? $this->light['data'] : [];
            $error = isset($this->light['error']) && is_array($this->light['error']) ? $this->light['error'] : [];
            return new APIResponse(true, $data, $error);
        }

        // --------------------------------------------------
        // ------------------- Rooms ------------------------
        // --------------------------------------------------
        if ($resource === 'room' && !$id && $authorise) {
            return $this->createAPIResponseFromFile('room.json');
        }

        if ($resource === 'room' && $id === RoomsApiRepositoryTest::TEST_ROOM_ID && $authorise) {
            return $this->createAPIResponseFromFile('room-by-id.json');
        }

        // --------------------------------------------------
        // ------------------- Scenes -----------------------
        // --------------------------------------------------
        if ($resource === 'scene' && !$id && $authorise) {
            return $this->createAPIResponseFromFile('scene.json');
        }

        if ($resource === 'scene' && $id === ScenesApiRepositoryTest::TEST_SCENE_ID && $authorise) {
            return $this->createAPIResponseFromFile('scene-by-id.json');
        }

        // --------------------------------------------------
        // ------------------- Zones ------------------------
        // --------------------------------------------------
        if ($resource === 'zone' && !$id && $authorise) {
            return $this->createAPIResponseFromFile('zone.json');
        }

        if ($resource === 'zone' && $id === ZonesApiRepositoryTest::TEST_ZONE_ID && $authorise) {
            return $this->createAPIResponseFromFile('zone-by-id.json');
        }

        return new APIResponse(false, [], []);
    }

    private function createAPIResponseFromFile(string $filename): APIResponse
    {
        $fileContent = file_get_contents(__DIR__ . '/' . $filename) ?: '';
        $fileData = (array)(json_decode($fileContent, true) ?: []);
        $data = isset($fileData['data']) && is_array($fileData['data']) ? $fileData['data'] : [];
        $error = isset($fileData['error']) && is_array($fileData['error']) ? $fileData['error'] : [];

        return new APIResponse(true, $data, $error);
    }

    public function updateResource(string $resource, string $id, array $data, bool $authorise = true): APIResponse
    {
        if ($resource === 'light' && $id === LightsApiRepositoryTest::TEST_LIGHT_ID && $authorise) {
            $this->light = array_replace_recursive($this->light, ['data' => [0 => $data]]);

            return new APIResponse(true, [[0 => ['rid' => $id, 'rtype' => 'light']]], []);
        }

        return new APIResponse(false, [], []);
    }

    public function deleteResource(string $resource, string $id, bool $authorise = true): APIResponse
    {
        return new APIResponse(false, [], []);
    }

    public function callAPIDirectly(string $address, string $method, ?array $data, bool $authorise = true): APIResponse
    {
        return new APIResponse(false, [], []);
    }
}
