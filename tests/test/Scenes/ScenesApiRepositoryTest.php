<?php

declare(strict_types=1);

namespace IoTHome\Tests\PhilipsHueApi\Scenes;

use IoTHome\PhilipsHueApi\Client\APIClientInterface;
use IoTHome\PhilipsHueApi\Lights\Color;
use IoTHome\PhilipsHueApi\Rooms\Room;
use IoTHome\PhilipsHueApi\Scenes\Action;
use IoTHome\PhilipsHueApi\Scenes\Scene;
use IoTHome\PhilipsHueApi\Scenes\ScenesApiRepository;
use IoTHome\PhilipsHueApi\Zones\Zone;
use IoTHome\PhilipsHueApi\Zones\ZonesApiRepository;
use IoTHome\Tests\PhilipsHueApi\Client\DummyAPIClient;
use PHPUnit\Framework\TestCase;

final class ScenesApiRepositoryTest extends TestCase
{
    public const TEST_SCENE_ID = 'abcdef12-abcd-1234-93bb-da3e0ea601d1';

    private function getAPIClient(): APIClientInterface
    {
        return new DummyAPIClient();
    }

    /**
     * @test
     */
    public function itGetsAll(): void
    {
        $repository = new ScenesApiRepository($this->getAPIClient());

        $collection = $repository->getAll();

        $this->assertEquals(6, count($collection));

        $first = reset($collection);

        $this->assertInstanceOf(Scene::class, $first);

        if ($first instanceof Scene) {
            $this->assertEquals('abcdef12-abcd-1234-93bb-da3e0ea601d1', $first->getId());
            $this->assertEquals('Reading', $first->getName());
            $this->assertEquals('e101a77f-9984-4f61-aac8-15741983c656', $first->getPublicImageId());
            $this->assertEquals(0.5, $first->getSpeed());

            $group = $first->getGroup();
            $this->assertInstanceOf(Room::class, $group);
            if ($group instanceof Room) {
                $this->assertEquals('abcdefgh-1234-abcd-a6d2-c2f41f56407e', $group->getId());
            }
            $this->assertEquals([
                'abcdef12-abcd-1234-a48c-7a009442db42',
                'abcdef12-abcd-1234-982b-ae15497773de'
            ], $group->getLightIds());

            $actions = $first->getActions();
            $this->assertEquals(2, count($actions));
            $this->assertEquals([
                new Action('abcdef12-abcd-1234-a48c-7a009442db42', true, new Color(0.445, 0.4067), 346, 100),
                new Action('abcdef12-abcd-1234-982b-ae15497773de', true, new Color(0.445, 0.4067), 346, 100),
            ], $actions);
        }
    }

    /**
     * @test
     */
    public function itGetsById(): void
    {
        $repository = new ScenesApiRepository($this->getAPIClient());

        $scene = $repository->getById(self::TEST_SCENE_ID);

        $this->assertInstanceOf(Scene::class, $scene);

        if ($scene instanceof Scene) {
            $this->assertEquals('abcdef12-abcd-1234-93bb-da3e0ea601d1', $scene->getId());
            $this->assertEquals('Reading', $scene->getName());
            $this->assertEquals('e101a77f-9984-4f61-aac8-15741983c656', $scene->getPublicImageId());
            $this->assertEquals(0.5, $scene->getSpeed());

            $group = $scene->getGroup();
            $this->assertInstanceOf(Room::class, $group);
            if ($group instanceof Room) {
                $this->assertEquals('abcdef12-abcd-1234-a6d2-c2f41f56407e', $group->getId());
            }
            $this->assertEquals([
                'abcdef12-abcd-1234-a48c-7a009442db42',
                'abcdef12-abcd-1234-982b-ae15497773de'
            ], $group->getLightIds());

            $actions = $scene->getActions();
            $this->assertEquals(2, count($actions));
            $this->assertEquals([
                new Action('abcdef12-abcd-1234-a48c-7a009442db42', true, new Color(0.445, 0.4067), 346, 100),
                new Action('abcdef12-abcd-1234-982b-ae15497773de', true, new Color(0.445, 0.4067), 346, 100),
            ], $actions);
        }
    }
}
