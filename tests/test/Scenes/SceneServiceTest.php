<?php

declare(strict_types=1);

namespace IoTHome\Tests\PhilipsHueApi\Scenes;

use IoTHome\PhilipsHueApi\Hue\Client;
use IoTHome\PhilipsHueApi\Lights\Color;
use IoTHome\PhilipsHueApi\Lights\Light;
use IoTHome\PhilipsHueApi\Lights\LightsApiRepository;
use IoTHome\PhilipsHueApi\Scenes\Action;
use IoTHome\PhilipsHueApi\Scenes\Scene;
use IoTHome\PhilipsHueApi\Scenes\SceneService;
use IoTHome\PhilipsHueApi\Zones\Zone;
use IoTHome\Tests\PhilipsHueApi\Client\DummyAPIClient;
use PHPUnit\Framework\TestCase;
use IoTHome\Tests\PhilipsHueApi\Lights\LightsApiRepositoryTest;

final class SceneServiceTest extends TestCase
{
    /**
     * @test
     */
    public function itUpdatesLightByScene(): void
    {
        $lightId = LightsApiRepositoryTest::TEST_LIGHT_ID;

        $zone = new Zone('', '', '', [$lightId]);

        $actions = [
            new Action(LightsApiRepositoryTest::TEST_LIGHT_ID, true, new Color(0.5, 0.6), 300, 10)
        ];

        $scene = new Scene('test', 'test', '', 0, $zone, $actions);

        $dummyClient = new DummyAPIClient();
        $lightsRepository = new LightsApiRepository($dummyClient);

        $client = $this->createMock(Client::class);

        $client->method('updateLight')->will(
            $this->returnCallback(function (Light $light) use ($lightsRepository) {
                return $lightsRepository->update($light);
            })
        );

        $client->method('getLights')->will(
            $this->returnCallback(function () use ($lightsRepository) {
                return $lightsRepository->getAll();
            })
        );

        $service = new SceneService($client);

        $service->applyScene($scene);

        $light = $lightsRepository->getById($lightId);

        $this->assertTrue($light->isOn());
        $this->assertEquals(300, $light->getColorTemperature());
        $this->assertEquals(10, $light->getBrightness());
        $this->assertEquals(0.5, $light->getX());
        $this->assertEquals(0.6, $light->getY());
    }
}
