<?php

declare(strict_types=1);

namespace IoTHome\Tests\PhilipsHueApi\Scenes;

use IoTHome\PhilipsHueApi\Lights\Color;
use IoTHome\PhilipsHueApi\Rooms\Room;
use IoTHome\PhilipsHueApi\Scenes\Action;
use IoTHome\PhilipsHueApi\Scenes\Scene;
use IoTHome\PhilipsHueApi\Scenes\SceneFactory;
use IoTHome\PhilipsHueApi\Zones\Zone;
use PHPUnit\Framework\TestCase;

final class SceneFactoryTest extends TestCase
{
    /**
     * @test
     */
    public function itCreatesSceneWithZone(): void
    {
        $data = [
            'id' => 'abcd',
            'metadata' => [
                'name' => 'Test Scene',
                'image' => [
                    'rtype' => 'public_image',
                    'rid' => 'cfdc'
                ]
            ],
            'speed' => 0.5,
            'group' => [
                'rtype' => 'zone',
                'rid' => 'aiud'
            ],
            'actions' => [
                [
                    'action' => [
                        'dimming' => ['brightness' => 10],
                        'color_temperature' => ['mirek' => 10],
                        'color' => ['xy' => ['x' => 0.5, 'y' => 0.6]]
                    ],
                    'target' => [
                        'rtype' => 'light',
                        'rid' => 'a1'

                    ]
                ],
                [
                    'action' => [
                        'dimming' => ['brightness' => 20],
                        'color_temperature' => ['mirek' => 20]
                    ],
                    'target' => [
                        'rtype' => 'light',
                        'rid' => 'a2'
                    ]
                ]
            ],
        ];

        $factory = new SceneFactory();
        $scene = $factory->createFromApiData($data);

        $this->assertInstanceOf(Scene::class, $scene);
        $this->assertEquals('abcd', $scene->getId());
        $this->assertEquals('Test Scene', $scene->getName());
        $this->assertEquals(0.5, $scene->getSpeed());
        $this->assertEquals('cfdc', $scene->getPublicImageId());

        $group = $scene->getGroup();
        $this->assertInstanceOf(Zone::class, $group);
        if ($group instanceof Zone) {
            $this->assertEquals('aiud', $group->getId());
        }
        $this->assertEquals(['a1', 'a2'], $group->getLightIds());

        $actions = $scene->getActions();
        $this->assertEquals(2, count($actions));
        $this->assertEquals([
            new Action('a1', true, new Color(0.5, 0.6), 10, 10),
            new Action('a2', true, null, 20, 20),
        ], $actions);
    }

    /**
     * @test
     */
    public function itCreatesSceneWithRoom(): void
    {
        $data = [
            'id' => 'abcd',
            'metadata' => [
                'name' => 'Test Scene',
                'image' => [
                    'rtype' => 'public_image',
                    'rid' => 'cfdc'
                ]
            ],
            'speed' => 0.5,
            'group' => [
                'rtype' => 'room',
                'rid' => 'aiud'
            ],
            'actions' => [
                [
                    'action' => [
                        'dimming' => ['brightness' => 10],
                        'color_temperature' => ['mirek' => 10],
                        'color' => ['xy' => ['x' => 0.5, 'y' => 0.6]]
                    ],
                    'target' => [
                        'rtype' => 'light',
                        'rid' => 'a1'

                    ]
                ],
                [
                    'action' => [
                        'dimming' => ['brightness' => 20],
                        'color_temperature' => ['mirek' => 20]
                    ],
                    'target' => [
                        'rtype' => 'light',
                        'rid' => 'a2'
                    ]
                ]
            ],
        ];

        $factory = new SceneFactory();
        $scene = $factory->createFromApiData($data);

        $this->assertInstanceOf(Scene::class, $scene);
        $this->assertEquals('abcd', $scene->getId());
        $this->assertEquals('Test Scene', $scene->getName());
        $this->assertEquals(0.5, $scene->getSpeed());
        $this->assertEquals('cfdc', $scene->getPublicImageId());

        $group = $scene->getGroup();
        $this->assertInstanceOf(Room::class, $group);
        if ($group instanceof Room) {
            $this->assertEquals('aiud', $group->getId());
        }
        $this->assertEquals(['a1', 'a2'], $group->getLightIds());

        $actions = $scene->getActions();
        $this->assertEquals(2, count($actions));
        $this->assertEquals([
            new Action('a1', true, new Color(0.5, 0.6), 10, 10),
            new Action('a2', true, null, 20, 20),
        ], $actions);
    }
}
