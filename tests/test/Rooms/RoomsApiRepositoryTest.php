<?php

declare(strict_types=1);

namespace IoTHome\Tests\PhilipsHueApi\Rooms;

use IoTHome\PhilipsHueApi\Client\APIClientInterface;
use IoTHome\PhilipsHueApi\Rooms\Room;
use IoTHome\PhilipsHueApi\Rooms\RoomsApiRepository;
use IoTHome\Tests\PhilipsHueApi\Client\DummyAPIClient;
use PHPUnit\Framework\TestCase;

final class RoomsApiRepositoryTest extends TestCase
{
    public const TEST_ROOM_ID = 'abcdefgh-1234-abcd-a6d2-c2f41f56407e';

    private function getAPIClient(): APIClientInterface
    {
        return new DummyAPIClient();
    }

    /**
     * @test
     */
    public function itGetsAll(): void
    {
        $repository = new RoomsApiRepository($this->getAPIClient());

        $collection = $repository->getAll();

        $this->assertEquals(2, count($collection));

        $first = reset($collection);

        $this->assertInstanceOf(Room::class, $first);

        if ($first instanceof Room) {
            $this->assertEquals('abcdefgh-1234-abcd-a6d2-c2f41f56407e', $first->getId());
            $this->assertEquals('Living room', $first->getName());
            $this->assertEquals('living_room', $first->getArchetype());
            $this->assertEquals(
                ['abcdefgh-1234-abcd-a48c-7a009442db42', 'abcdefgh-1234-abcd-982b-ae15497773de'],
                $first->getLightIds()
            );
        }
    }

    /**
     * @test
     */
    public function itGetsById(): void
    {
        $repository = new RoomsApiRepository($this->getAPIClient());

        $room = $repository->getById(self::TEST_ROOM_ID);

        $this->assertInstanceOf(Room::class, $room);
        $this->assertEquals('abcdefgh-1234-abcd-a6d2-c2f41f56407e', $room->getId());
        $this->assertEquals('Living room', $room->getName());
        $this->assertEquals('living_room', $room->getArchetype());
        $this->assertEquals(
            ['abcdefgh-1234-abcd-a48c-7a009442db42', 'abcdefgh-1234-abcd-982b-ae15497773de'],
            $room->getLightIds()
        );
    }
}
