<?php

declare(strict_types=1);

namespace IoTHome\Tests\PhilipsHueApi\Rooms;

use IoTHome\PhilipsHueApi\Rooms\Room;
use IoTHome\PhilipsHueApi\Rooms\RoomFactory;
use PHPUnit\Framework\TestCase;

final class RoomFactoryTest extends TestCase
{
    /**
     * @test
     */
    public function itCreatesRoom(): void
    {
        $data = [
            'id' => 'abcd',
            'metadata' => [
                'name' => 'test',
                'archetype' => 'office',
            ],
            'services' => [
                [
                    'rid' => 'a1',
                    'rtype' => 'light',
                ],
                [
                    'rid' => 'a2',
                    'rtype' => 'light',
                ],
                [
                    'rid' => 'b1',
                    'rtype' => 'grouped_light',
                ],
            ]
        ];

        $factory = new RoomFactory();
        $room = $factory->createFromApiData($data);

        $this->assertInstanceOf(Room::class, $room);
        $this->assertEquals('abcd', $room->getId());
        $this->assertEquals('test', $room->getName());
        $this->assertEquals('office', $room->getArchetype());
        $this->assertEquals(['a1', 'a2'], $room->getLightIds());
    }
}
